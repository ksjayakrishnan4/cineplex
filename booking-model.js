class Model {
    constructor() {

        this.contacts = []

    }

    getContacts() {
        return this.contacts;
    }

    setContacts(contact) {
        this.contacts.push(contact)

        localStorage.setItem('contacts', JSON.stringify(this.contacts))
        console.log(localStorage.getItem('contacts'));
    }

}

export default Model;
