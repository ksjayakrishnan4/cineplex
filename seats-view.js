import Controller from "./seats-controller.js";
const controller = new Controller()
function render () {
 
    const rootDiv = document.getElementById('root')
    rootDiv.innerHTML = '';
    let seats = controller.getSeats()
    
    for (let i = 0; i <seats.length; i++) {
    

        const newSeat = document.createElement('div')
        newSeat.classList.add('seat')
        newSeat.dataset.seatNumber = i;
    
        if (seats[i] === 'Booked') {
            newSeat.classList.add('booked')   
        }
        if (seats[i] === 'selected') {
            newSeat.classList.add('Selected')   
        }
        newSeat.addEventListener('click' , (event) => {
            controller.selectSeat(Number(newSeat.dataset.seatNumber))
            render()
        })
        rootDiv.appendChild(newSeat)
    }
    
}

render()

const button = document.getElementById('btn')
    button.addEventListener('click' , (e) => {
        controller.bookSeats()
        window.location.href = './booking.html'
        render()
    })
