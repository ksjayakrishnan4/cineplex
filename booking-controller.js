import Model from "./booking-model.js";

class Controller {
    constructor() {

        this.model = new Model();

    }

    getContacts() {
        return this.model.getContacts();
    }

    setContacts(name, email, number) {

        let isvalid = true

        if (name.length <= 0 || name.length >= 15  )  {
            isvalid = false
            console.log("please fill this column")
            return
        }
        if (number.length <=10 && number.length >= 12 ) {
            isvalid = false
            console.log("enter a valid contact number")
            return
        }

        var values = {
            name : name,
            email : email,
            number : number
        }

        this.model.setContacts(values)

    }
}

export default Controller;
